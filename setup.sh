#!/bin/bash

echo [include] >> ~/.gitconfig
echo "	path = $(pwd)/alias.inc" >> ~/.gitconfig

echo [pull] >> ~/.gitconfig
echo "	rebase = false" >> ~/.gitconfig
